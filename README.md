
# OpenSSL Public Key Infrastructure (PKI)

## Database Creation (Clear)
```bash
cp /dev/null ca/root-ca/db/root-ca.db
cp /dev/null ca/root-ca/db/root-ca.db.attr
echo 01 > ca/root-ca/db/root-ca.crt.srl
echo 01 > ca/root-ca/db/root-ca.crl.srl
cp /dev/null ca/signing-ca/db/signing-ca.db
cp /dev/null ca/signing-ca/db/signing-ca.db.attr
echo 01 > ca/signing-ca/db/signing-ca.crt.srl
echo 01 > ca/signing-ca/db/signing-ca.crl.srl
cp /dev/null ca/sysop-ca/db/sysop-ca.db
cp /dev/null ca/sysop-ca/db/sysop-ca.db.attr
echo 01 > ca/sysop-ca/db/sysop-ca.crt.srl
echo 01 > ca/sysop-ca/db/sysop-ca.crl.srl
```

##  Root CA Generation
```bash
openssl req -new -config conf/root-ca.conf -out csr/root-ca.csr -keyout keys/root-ca.key
openssl ca -selfsign -config conf/root-ca.conf -in csr/root-ca.csr -out certs/root-ca.crt \
-extensions root_ca_ext -startdate 200101000000Z -enddate 400101000010Z
```

![Root](./.screens/root-ca.png "Root")

## CA Generation
```bash
openssl req -new -config conf/signing-ca.conf -out csr/signing-ca.csr -keyout keys/signing-ca.key
openssl ca -config conf/root-ca.conf -in csr/signing-ca.csr -out certs/signing-ca.crt \
-extensions signing_ca_ext -startdate 200101000000Z -enddate 300101000000Z
```

![CA](./.screens/sub-ca.png "CA")

## Users Generation
```bash
openssl req -new -config conf/identity.conf -out csr/user.csr -keyout keys/user.key
openssl ca -config conf/signing-ca.conf -in csr/user.csr -out certs/user.crt \
-extensions identity_ext -startdate 200101000000Z -enddate 210101000000Z
openssl pkcs12 -export -name "User" -inkey keys/user.key -in certs/user.crt -out p12/user.p12
```

![User](./.screens/user.png "User")

## Server Generation
```bash
export SAN="DNS:www.domaine.wtf, DNS:domaine.wtf"
openssl req -new -config conf/server.conf -out csr/www.domaine.wtf.csr -keyout keys/www.domaine.wtf.key
openssl ca -config conf/signing-ca.conf -in csr/www.domaine.wtf.csr -out certs/www.domaine.wtf.crt \
-extensions server_ext -startdate 200101000000Z -enddate 230101000000Z
```

![Server](./.screens/server.png "Server")

## CRL Generation
```bash
openssl ca -gencrl -config conf/signing-ca.conf -out crl/signing-ca.crl
openssl ca -gencrl -config conf/root-ca.conf -out crl/root-ca.crl
```

## Revocation
```bash
openssl ca -config conf/signing-ca.conf -revoke ca/signing-ca/01.pem -crl_reason superseded
```
