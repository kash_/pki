# Simple Signing CA

# The [default] section contains global constants that can be referred to from
# the entire configuration file. It may also hold settings pertaining to more
# than one openssl command.

[ default ]
ca					= signing-ca						# CA name
dir					= .							# Top dir

# The next part of the configuration file is used by the openssl req command.
# It defines the CA's key pair, its DN, and the desired extensions for the CA
# certificate.

[ req ]
default_bits				= 4096							# RSA key size
encrypt_key				= yes							# Protect private key
default_md				= sha512						# MD to use
utf8					= yes							# Input is UTF-8
string_mask				= utf8only						# Emit UTF-8 strings
prompt					= no							# Don't prompt for DN
distinguished_name			= ca_dn							# DN section
req_extensions				= ca_reqext						# Desired extensions

[ ca_dn ]
countryName				= "CA"
stateOrProvinceName			= "Quebec"
localityName				= "Montreal"
organizationName			= "WTF"
organizationalUnitName			= "CA"
commonName				= "WTF CA"
emailAddress				= "hostmaster@domaine.wtf"

[ ca_reqext ]
basicConstraints			= critical,CA:true,pathlen:0
keyUsage				= critical,keyCertSign,cRLSign
subjectKeyIdentifier			= hash

# The remainder of the configuration file is used by the openssl ca command.
# The CA section defines the locations of CA assets, as well as the policies
# applying to the CA.

[ ca ]
default_ca				= signing_ca						# The default CA section

[ signing_ca ]
certificate				= $dir/certs/$ca.crt					# The CA cert
private_key				= $dir/keys/$ca.key					# CA private key
new_certs_dir				= $dir/ca/$ca						# Certificate archive
serial					= $dir/ca/$ca/db/$ca.crt.srl				# Serial number file
crlnumber				= $dir/ca/$ca/db/$ca.crl.srl				# CRL number file
database				= $dir/ca/$ca/db/$ca.db					# Index file
unique_subject				= no							# Require unique subject
default_days				= 731							# How long to certify for
default_md				= sha512						# MD to use
policy					= match_pol						# Default naming policy
email_in_dn				= yes							# Add email to cert DN
preserve				= no							# Keep passed DN ordering
name_opt				= ca_default						# Subject DN display options
cert_opt				= ca_default						# Certificate display options
copy_extensions				= copy							# Copy extensions from CSR
x509_extensions				= identity_ext						# Default cert extensions
default_crl_days			= 2							# How long before next CRL
crl_extensions				= crl_ext						# CRL extensions

# Naming policies control which parts of a DN end up in the certificate and
# under what circumstances certification should be denied.

[ match_pol ]
countryName				= supplied
stateOrProvinceName			= supplied
localityName				= supplied
organizationName			= supplied
organizationalUnitName			= supplied
commonName				= supplied
emailAddress				= supplied

[ any_pol ]
domainComponent				= optional

# Certificate extensions define what types of certificates the CA is able to
# create.

[ identity_ext ]
basicConstraints			= CA:false
keyUsage				= critical,digitalSignature,keyEncipherment,nonRepudiation
extendedKeyUsage			= emailProtection,clientAuth,msSmartcardLogin
subjectKeyIdentifier			= hash
authorityKeyIdentifier			= keyid:always
nsCertType				= client
subjectAltName				= email:copy

[ server_ext ]
basicConstraints			= CA:false
keyUsage				= critical,digitalSignature,keyEncipherment
extendedKeyUsage			= serverAuth
authorityKeyIdentifier			= keyid:always
subjectKeyIdentifier			= hash
nsCertType				= server

# CRL extensions exist solely to point to the CA certificate that has issued
# the CRL.

[ crl_ext ]
authorityKeyIdentifier			= keyid:always
